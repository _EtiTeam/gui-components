CREATE TABLE WarehouseBooks (
	ID int PRIMARY KEY IDENTITY,
	Title varchar(255) NOT NULL,
	Author varchar(255) NOT NULL,
	ReleaseYear decimal(4,0) NOT NULL,
	ISBN int NOT NULL, 
	Price int NOT NULL,
	Amount int NOT NULL
)

INSERT INTO WarehouseBooks VALUES ('The Bible', 'Unknown', 1960, 132456789, 50, 90);
INSERT INTO WarehouseBooks VALUES ('Fairytales', 'Hans Andersen', 1890, 456123789, 75,90);
INSERT INTO WarehouseBooks VALUES ('Edgar Alan Poe', 'HorrorBook', 1990, 789654132, 40, 90);
INSERT INTO WarehouseBooks VALUES ('Winnie The Pooth', 'Alan Milne', 1950, 711654132, 40, 90);
