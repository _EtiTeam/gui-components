CREATE TABLE Stores (
	ID int PRIMARY KEY IDENTITY,
	City varchar(255) NOT NULL
)

CREATE TABLE Drivers (
	ID int PRIMARY KEY IDENTITY,
	Name varchar(255) NOT NULL,
	Surname varchar(255) NOT NULL,
	Capacity int NOT NULL
)

CREATE TABLE Delivery (
	ID int PRIMARY KEY IDENTITY,
	DriverID int REFERENCES Drivers,
	StoreID int REFERENCES Stores,
	OrderID int, /*ref orders table*/
	DeliveryState varchar(255) NOT NULL, 
	DeliveryTime DateTime NOT NULL,
	Duration int NOT NULL
)

INSERT INTO Stores VALUES ('London');
INSERT INTO Stores VALUES ('Manchester');
INSERT INTO Stores VALUES ('Crewe');

INSERT INTO Drivers VALUES ('Frank', 'Gesek',55);
INSERT INTO Drivers VALUES ('Elivs', 'Krawczyk',33);
INSERT INTO Drivers VALUES ('Neil', 'Cugowski',120);
INSERT INTO Drivers VALUES ('Gerry', 'Kowalski',150);

INSERT INTO Delivery VALUES (1,1,2,'WAITING','2017-07-20 12:12:12',14);
INSERT INTO Delivery VALUES (2,1,3,'WAITING','2017-07-21 15:14:42',12);
