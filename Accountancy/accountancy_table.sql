CREATE TABLE Payments (
	id int PRIMARY KEY IDENTITY,
	OrderID int, /*ref orders table*/
	Value int NOT NULL,
	PaymentState varchar(255) NOT NULL, 
	PaymentDate DateTime NOT NULL
)

INSERT INTO Payments VALUES (123, 50, 'DONE', '2017-07-10 12:01:49'); 
INSERT INTO Payments VALUES (345, 75, 'DONE', '2017-07-13 15:11:23'); 
INSERT INTO Payments VALUES (567, 30, 'WAITING', '2017-07-18 10:21:23'); 