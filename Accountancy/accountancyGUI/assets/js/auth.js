const localPath = (location.href).substr(0, (location.href).lastIndexOf('/'));
const servicePath = 'http://localhost/accountancyService/rest';
const userServicePath = 'http://localhost/accountancyService/users/rest';

function logout() {
	sessionStorage.removeItem('UUID');
	sessionStorage.removeItem('direction');
	location.reload();
}

function trySignIn() {
	if (sessionStorage.getItem('UUID') != null){
		path = servicePath + "/index.html";
		httpGetAsync(path, loadPage);
	}
}

function isSignedIn() {
	var verifySignedInPath = userServicePath + '/isSignedIn';
	var xhr = new XMLHttpRequest();
	xhr.open('POST', verifySignedInPath);	
    xhr.onload = function () {
	   if (xhr.responseText == 'true') {
		   trySignIn();
	   } 
    };	
	xhr.send(sessionStorage.getItem('UUID'));	
}

function signIn(){
	sessionStorage.removeItem('UUID');
	var xhr = new XMLHttpRequest();
	xhr.open('POST', userServicePath + '/signin');
		
	var userLogin = document.getElementById('login').value;
	var userPassword = document.getElementById('password').value;
	var JSONdata = {};
	JSONdata.login = userLogin;
	JSONdata.password = userPassword;
	var JSONarray = new Array();
	JSONarray[0] = JSONdata;
	
    xhr.onload = function () {
	    //insertData(xhr.responseText);
	    sessionStorage.setItem('login', userLogin);
	    sessionStorage.setItem('UUID', xhr.responseText);
	    path = servicePath + "/index.html";
		httpGetAsync(path, loadPage);
    };
	
	xhr.send(JSON.stringify(JSONarray));		
}


function httpGetAsync(path, callback)
{
    var xmlHttp = new XMLHttpRequest();
	

    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", path, true); // true for asynchronous 
	xmlHttp.setRequestHeader('Authorization', sessionStorage.getItem('UUID'));
    xmlHttp.send(null);
}

function loadPage(content) {
	document.open();
	document.write(content);
	document.close();
}

function navigateTo(direction){
	switch(direction){
		case 'Orders':
			sessionStorage.setItem('direction', direction);
			path = servicePath + "/subpage.html";
			httpGetAsync(path, loadPage);
			break;
		case 'Customers':
			sessionStorage.setItem('direction', direction);
			path = servicePath + "/subpage.html";
			httpGetAsync(path, loadPage);
			break;
		case 'Books':
			sessionStorage.setItem('direction', direction);
			path = servicePath + "/subpage.html";
			httpGetAsync(path, loadPage);
			break;
		case 'Payments':
			sessionStorage.setItem('direction', direction);
			path = servicePath + "/subpage.html";
			httpGetAsync(path, loadPage);
			break;		
	}
	
}
