CREATE TABLE Books (
	ID int PRIMARY KEY IDENTITY,
	Title varchar(255) NOT NULL,
	Author varchar(255) NOT NULL,
	ReleaseYear decimal(4,0) NOT NULL,
	ISBN int NOT NULL, 
	Price int NOT NULL,
	Amount int NOT NULL
)

CREATE TABLE Customers (
	ID int PRIMARY KEY IDENTITY,
	Name varchar(255) NOT NULL,
	Surname varchar(255) NOT NULL
)

CREATE TABLE Orders (
	ID int PRIMARY KEY IDENTITY,
	CustomerID int REFERENCES Customers,
	BookID int REFERENCES Books,
	OrderState varchar(255) NOT NULL, 
	OrderDate DateTime NOT NULL
)

INSERT INTO Books VALUES ('The Bible', 'Unknown', 1960, 132456789, 50, 13);
INSERT INTO Books VALUES ('Fairytales', 'Hans Andersen', 1890, 456123789, 75, 5);
INSERT INTO Books VALUES ('Edgar Alan Poe', 'HorrorBook', 1990, 789654132, 40, 8);

INSERT INTO Customers VALUES ('Frank', 'Sinatra');
INSERT INTO Customers VALUES ('Elivs', 'Presley');
INSERT INTO Customers VALUES ('Neil', 'Armstrong');
INSERT INTO Customers VALUES ('Gerry', 'Raffrerty');

INSERT INTO Orders VALUES (2,1,'WAITING','2017-07-08 12:12:12');
INSERT INTO Orders VALUES (1,1,'WAITING','2017-07-05 15:14:42');
INSERT INTO Orders VALUES (3,1,'WAITING','2017-07-06 11:13:10');